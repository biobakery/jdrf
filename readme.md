# JDRF bioinformatics project #

The JDRF bioinformatics project began in 2015/2016 to provide a high-performance, highly available computing infrastructure for the automated analyses of T1D research data.  The work in this repository provides a framework for rapid testing, deployment, and maintenance of bioinformatics tools.
 
